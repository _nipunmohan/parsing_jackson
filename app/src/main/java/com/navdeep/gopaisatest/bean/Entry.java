
package com.navdeep.gopaisatest.bean;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "im:name",
    "im:image",
    "summary",
    "im:price"
})
public class Entry {

    @JsonProperty("im:name")
    private ImName imName;
    @JsonProperty("im:image")
    private List<ImImage> imImage = null;
    @JsonProperty("summary")
    private Summary summary;
    @JsonProperty("im:price")
    private ImPrice imPrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("im:name")
    public ImName getImName() {
        return imName;
    }

    @JsonProperty("im:name")
    public void setImName(ImName imName) {
        this.imName = imName;
    }

    @JsonProperty("im:image")
    public List<ImImage> getImImage() {
        return imImage;
    }

    @JsonProperty("im:image")
    public void setImImage(List<ImImage> imImage) {
        this.imImage = imImage;
    }

    @JsonProperty("summary")
    public Summary getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    @JsonProperty("im:price")
    public ImPrice getImPrice() {
        return imPrice;
    }

    @JsonProperty("im:price")
    public void setImPrice(ImPrice imPrice) {
        this.imPrice = imPrice;
    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package com.navdeep.gopaisatest.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.navdeep.gopaisatest.AppController;
import com.navdeep.gopaisatest.R;
import com.navdeep.gopaisatest.adapter.FeedAdapter;
import com.navdeep.gopaisatest.bean.ListingResponse;
import com.navdeep.gopaisatest.httprequest.JacksonRequest;
import com.navdeep.gopaisatest.utils.AppConstant;

public class MainActivity extends AppCompatActivity {


    private String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog pDialog;

    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        getFeeds();
    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private void getFeeds() {
        showProgressDialog();

        AppController.getInstance().addToRequestQueue(new JacksonRequest<ListingResponse>(ListingResponse.class,
                Request.Method.GET, null, AppConstant.LISTING_API, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                hideProgressDialog();
                if (response != null) {
                    ListingResponse listingResponse = (ListingResponse) response;
                    Log.d(TAG, response.toString());
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    recyclerView.setAdapter(new FeedAdapter(MainActivity.this, listingResponse.getFeed().getEntry()));
                }
            }
        }, new JacksonRequest.FailureListener() {
            @Override
            public void onRequestFailure(int code, String message) {
                hideProgressDialog();

                Snackbar.make(coordinatorLayout, getString(R.string.unable_to_connect_server), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.d(TAG, "onRequestFailure " + code + " : " + message);
            }
        }));
    }
}

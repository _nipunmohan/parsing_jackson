package com.navdeep.gopaisatest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.navdeep.gopaisatest.R;
import com.navdeep.gopaisatest.bean.Entry;

import java.util.ArrayList;


public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {


    private Context context;
    private ArrayList<Entry> entry;

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_item_layout, parent, false);

        return new FeedAdapter.FeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        Entry mEntry = entry.get(position);
        if (mEntry.getImName() != null && mEntry.getImName().getLabel() != null) {
            holder.textViewName.setText(entry.get(position).getImName().getLabel());
        }

        if (mEntry.getImPrice() != null && mEntry.getImPrice().getLabel() != null) {
            holder.textViewPrice.setText(entry.get(position).getImPrice().getAttributes().getCurrency()
                    + " " + entry.get(position).getImPrice().getAttributes().getAmount());
        }

        if (mEntry.getSummary() != null && mEntry.getSummary().getLabel() != null) {
            holder.textViewSummary.setText(entry.get(position).getSummary().getLabel());
        }

        if (mEntry.getImImage() != null && mEntry.getImImage().size() > 0) {
            Glide.with(context).load(mEntry.getImImage().get(0).getLabel())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return entry.size();
    }


    public class FeedViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName, textViewPrice, textViewSummary;
        ImageView imageView;

        public FeedViewHolder(View view) {
            super(view);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewPrice = (TextView) view.findViewById(R.id.textViewPrice);
            textViewSummary = (TextView) view.findViewById(R.id.textViewSummary);
            imageView = (ImageView) view.findViewById(R.id.imageView);
        }
    }


    public FeedAdapter(Context context, ArrayList<Entry> entry) {
        this.context = context;
        this.entry = entry;
    }


}